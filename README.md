# Hexagonal Architecture DDD Spring Boot Application
Cette application est un exemple d'architecture hexagonale utilisant le Domain-Driven Design (DDD) avec Spring Boot. Elle simule une marketplace avec des entités Product, Order et Store, expose des endpoints REST et intègre Kafka pour la gestion des événements.

# Prérequis
Assurez-vous d'avoir installé les éléments suivants sur votre machine :

* Java 17 ou plus récent
* Docker
* Docker Compose
* Maven

# Structure du Projet
- domain: Contient les modèles de domaine et les services.
- application: Contient les ports de l'application (input et output).
- infrastructure: Contient les adaptateurs, les configurations et les entités de persistance.


# Configuration
Fichier application.yml
<br>
- Le fichier de configuration application.yml doit se trouver dans src/main/resources et doit contenir la configuration pour la base de données H2 en mémoire, Hibernate, Kafka et Actuator.

# Docker Configuration
* Dockerfile:
Ce fichier Dockerfile trouvé à la racine du projet est utilisé pour construire l'image Docker de l'application.

* docker-compose.yml:
ce fichier est utilisé pour configurer et lancer les services nécessaires, y compris Zookeeper, Kafka et l'application(optionnel).
<br>

# Construction et Exécution
# # Étapes pour Construire
- Cloner le dépôt :

Utilisez git clone [<https://gitlab.com/jmal.houssem98/kata-java-1>](https://gitlab.com/jmal.houssem98/kata-java-1) et naviguez dans le répertoire du projet.
- Construire le projet avec Maven 
- Utilisez mvn clean package pour construire le projet.
- Construire l'image Docker :
- Utilisez docker-compose up --build pour construire les images Docker.
<br>

# # Étapes pour Exécuter

- Utilisez docker-compose up pour démarrer les services nécessaires et l'application (kafka et zookeeper).
- start the project 
- L'application sera disponible sur http://localhost:8080.

# Endpoints REST
- POST /v1/stores: Crée un nouveau magasins.
- GET /v1/allstores: Récupère la liste de tous les magasins.

- POST /v1/products: Crée un nouveau produit.
- GET /v1/allproducts: Récupère la liste de tous les produits.
- GET /v1/products/{id} :Récupère un produit avec son id.

- POST /v1/orders: Crée une nouvelle commande.
- GET /v1/allorders: Récupère la liste de toutes les commandes.


# Tests
- Tests Unitaires et d'Intégration<br>
Pour exécuter les tests unitaires et d'intégration, utilisez la commande mvn test ou bien lancer les test manuellement.

# Tests de Performance
Pour exécuter les tests de performance, vous pouvez utiliser des outils comme JMeter ou bien exécuter le code développé (BenchmarkRunner)

# Monitoring
Spring Boot Actuator est utilisé pour les métriques et le monitoring. Vous pouvez accéder aux métriques via l'endpoint 
- http://localhost:8080/actuator/metrics
- http://localhost:8080/actuator/health

# KPI
L'application suit le nombre total de commandes créées et expose cette information via les endpoints de l'Actuator.

- http://localhost:8080/actuator/metrics/orders.created.total

Exemple de Réponse via Postman:
<br>
![alt text](image-1.png)


# Scénario d'Utilisation
- Étape 1 : Créer un Magasin<br>
![alt text](image-2.png)
<br>
- Étape 2 : Ajouter des Produits au Magasin<br>
![alt text](image-3.png)

- Étape 3 : Passer une Commande<br>
![<alt text>](image-4.png)
<br>
- Étape 5 : Vérifier les Commandes Passées ou bien consulter les produits et les magasins enregistrés

## Gestion des Commandes et de l'Inventaire avec Kafka
Lorsque l'application reçoit une nouvelle commande, les étapes suivantes sont effectuées :

1- Création de la Commande :

Un utilisateur envoie une requête pour créer une nouvelle commande en spécifiant le produit et la quantité souhaitée.<br>
L'application traite la requête et enregistre la commande dans la base de données.

2- Publication d'un Événement Kafka :

Après la création de la commande, l'application publie un événement Kafka contenant l'ID du produit commandé. <br>
Cet événement est envoyé à un topic Kafka spécifique dédié aux commandes.

3- Consommation de l'Événement Kafka :

Un consommateur Kafka intégré dans l'application est configuré pour écouter le topic des commandes.<br>
Lorsqu'un nouvel événement est reçu, le consommateur Kafka extrait l'ID du produit et la quantité de l'événement.<br>
Le consommateur Kafka utilise l'ID du produit pour rechercher toutes les commandes associées à ce produit dans la base de données.<br>
Il calcule la somme totale des quantités commandées pour ce produit en additionnant les quantités de toutes les commandes trouvées.<br>
L'inventaire est mis à jour en conséquence, reflétant la quantité totale de ce produit commandé.<br>


on voit ci-dessous une exemple :
![alt text](image.png)