package com.martinachov.hexagonal.infrastructure.adapters.input;


import com.martinachov.hexagonal.application.ports.input.CreateStoreUseCase;
import com.martinachov.hexagonal.application.ports.input.GetAllStoresUseCase;
import com.martinachov.hexagonal.domain.model.Store;
import com.martinachov.hexagonal.infrastructure.adapters.input.rest.data.request.StoreRequest;
import com.martinachov.hexagonal.infrastructure.adapters.input.rest.data.response.StoreResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class StoreRestAdapter {

    private final GetAllStoresUseCase getAllStoresUseCase;

    private final CreateStoreUseCase createStoreUseCase;

    private final ModelMapper mapper;

    @PostMapping(value = "/stores")
    public ResponseEntity<StoreResponse> createStore(@RequestBody StoreRequest productToCreate){
        // Request to domain
        Store store = mapper.map(productToCreate, Store.class);
        store = createStoreUseCase.createStore(store);
        // Domain to response
        return new ResponseEntity<>(mapper.map(store, StoreResponse.class), HttpStatus.CREATED);
    }

    @GetMapping(value = "/allstores")
    public List<StoreResponse> getAllStores() {
        return getAllStoresUseCase.getAllStores().stream()
                .map(store -> mapper.map(store, StoreResponse.class))
                .collect(Collectors.toList());
    }
}
