package com.martinachov.hexagonal.infrastructure.adapters.input.kafka;

import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class OrderKafkaConsumer {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderKafkaConsumer(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public int getTotalQuantity(Long productId) {
        return orderRepository.findTotalQuantityByProductId(productId).orElse(0);
    }

    @KafkaListener(topics = "orders", groupId = "group_id")
    public void consume(String message) {
        System.out.println("Consumed message: " + message);
        Long productId = Long.parseLong(message.trim());
        // Get total quantity for the productId
        int totalQuantity = getTotalQuantity(productId);
        System.out.println("Total quantity for productId " + productId + " is: " + totalQuantity);

    }
}