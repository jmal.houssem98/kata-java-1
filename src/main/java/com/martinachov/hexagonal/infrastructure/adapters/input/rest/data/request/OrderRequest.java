package com.martinachov.hexagonal.infrastructure.adapters.input.rest.data.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {
    private Long productId;
    private int quantity;
}
