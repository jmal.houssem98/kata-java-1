package com.martinachov.hexagonal.infrastructure.adapters.input.rest.data.request;


import lombok.*;

import javax.validation.constraints.NotEmpty;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoreRequest {
    @NotEmpty(message = "Name may not be empty")
    private String name;

    @NotEmpty(message = "location may not be empty")
    private String location;
}
