package com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity;

import com.martinachov.hexagonal.domain.model.Order;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private ProductEntity product;

    private int quantity;

    public OrderEntity(Order order) {
        this.id = order.getId();
        this.product = new ProductEntity(order.getProduct()); // Assuming ProductEntity constructor takes Product
        this.quantity = order.getQuantity();
    }
}
