package com.martinachov.hexagonal.infrastructure.adapters.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@OpenAPIDefinition(info = @Info(title = "API Documentation", version = "1.0", description = "API documentation for the application"))
public class SwaggerConfig   {

    @Bean
    public GroupedOpenApi atividadeApi() {
        return GroupedOpenApi.builder()
                .group("atividade-api")
                .packagesToScan("com.martinachov.hexagonal.infrastructure.adapters.input")
                .pathsToMatch("/**")
                .build();
    }
}
