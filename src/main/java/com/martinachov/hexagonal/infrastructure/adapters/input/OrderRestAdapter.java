package com.martinachov.hexagonal.infrastructure.adapters.input;

import com.martinachov.hexagonal.application.ports.input.CreateOrderUseCase;
import com.martinachov.hexagonal.application.ports.input.GetAllOrdersUseCase;
import com.martinachov.hexagonal.domain.model.Order;
import com.martinachov.hexagonal.infrastructure.adapters.input.rest.data.request.OrderRequest;
import com.martinachov.hexagonal.infrastructure.adapters.input.rest.data.response.OrderResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class OrderRestAdapter {

    private final CreateOrderUseCase createOrderUseCase;

    private final GetAllOrdersUseCase getAllOrdersUseCase;

    private final ModelMapper mapper;

    @PostMapping(value = "/orders")
    public ResponseEntity<OrderResponse> createOrder(@RequestBody OrderRequest orderToCreate){
        // Request to domain
        Order order = mapper.map(orderToCreate, Order.class);
        order = createOrderUseCase.createOrder(order);
        // Domain to response
        return new ResponseEntity<>(mapper.map(order, OrderResponse.class), HttpStatus.CREATED);
    }

    @GetMapping(value = "/allorders")
    public List<OrderResponse> getAllOrders() {
        return getAllOrdersUseCase.getAllOrders().stream()
                .map(product -> mapper.map(product, OrderResponse.class))
                .collect(Collectors.toList());
    }
}
