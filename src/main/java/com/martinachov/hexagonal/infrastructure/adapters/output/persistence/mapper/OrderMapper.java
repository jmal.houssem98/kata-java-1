package com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper;

import com.martinachov.hexagonal.domain.model.Order;
import com.martinachov.hexagonal.domain.model.Product;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.OrderEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.ProductEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class OrderMapper {
    @Autowired
    private ModelMapper mapper;

    public Order toOrder(OrderEntity entity){
        return mapper.map(entity, Order.class);
    }

    public OrderEntity toEntity(Order order){
        return mapper.map(order, OrderEntity.class);
    }
}
