package com.martinachov.hexagonal.infrastructure.adapters.config;

import com.martinachov.hexagonal.domain.service.OrderService;
import com.martinachov.hexagonal.domain.service.StoreService;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.OrderPersistenceAdapter;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.StorePersistenceAdapter;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper.OrderMapper;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper.StoreMapper;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.OrderRepository;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.StoreRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.martinachov.hexagonal.domain.service.ProductService;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.ProductPersistenceAdapter;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper.ProductMapper;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.ProductRepository;

/**
 * Configuracion BEANS
 */
@Configuration
public class BeanConfiguration {
    
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    //product
    @Bean
    public ProductMapper productMapper(){
        return new ProductMapper();
    }

    @Bean
    public ProductPersistenceAdapter productPersistenceAdapter(ProductRepository productRepository, ProductMapper productMapper) {
        return new ProductPersistenceAdapter(productRepository, productMapper);
    }

    @Bean
    public ProductService productService(ProductPersistenceAdapter productPersistenceAdapter) {
        return new ProductService(productPersistenceAdapter);
    }

    //store
    @Bean
    public StoreMapper storeMapper (){
        return new StoreMapper();
    }

    @Bean
    public StorePersistenceAdapter storePersistenceAdapter(StoreRepository storeRepository, StoreMapper storeMapper) {
        return new StorePersistenceAdapter(storeRepository, storeMapper);
    }

    @Bean
    public StoreService storeService(StorePersistenceAdapter storePersistenceAdapter) {
        return new StoreService(storePersistenceAdapter);
    }

    //order
    @Bean
    public OrderMapper orderMapper (){
        return new OrderMapper();
    }

    @Bean
    public OrderPersistenceAdapter orderPersistenceAdapter(OrderRepository orderRepository, OrderMapper orderMapper) {
        return new OrderPersistenceAdapter(orderRepository, orderMapper);
    }

    @Bean
    public OrderService orderService(OrderPersistenceAdapter orderPersistenceAdapter) {
        return new OrderService(orderPersistenceAdapter);
    }

}
