package com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository;

import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.OrderEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

    @Query("SELECT SUM(o.quantity) FROM OrderEntity o WHERE o.product.id = :productId")
    Optional<Integer> findTotalQuantityByProductId(Long productId);
}
