package com.martinachov.hexagonal.infrastructure.adapters.output.persistence;

import com.martinachov.hexagonal.application.ports.output.StoreOutputPort;
import com.martinachov.hexagonal.domain.model.Store;
import com.martinachov.hexagonal.infrastructure.adapters.output.kafka.OrderKafkaProducer;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.StoreEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper.StoreMapper;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class StorePersistenceAdapter implements StoreOutputPort {

    private final StoreRepository storeRepository;
    private final StoreMapper storeMapper;

    @Autowired
    private OrderKafkaProducer orderKafkaProducer;

    @Override
    public List<Store> getAllStores() {
        return storeRepository.findAll().stream()
                .map(storeMapper::toStore)
                .collect(Collectors.toList());
    }

    @Override
    public Store saveStore(Store store) {
        StoreEntity storeEntity = storeMapper.toEntity(store);
        storeRepository.save(storeEntity);
        return storeMapper.toStore(storeEntity);
    }
}
