package com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper;

import com.martinachov.hexagonal.domain.model.Product;
import com.martinachov.hexagonal.domain.model.Store;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.ProductEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.StoreEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class StoreMapper {
    @Autowired
    private ModelMapper mapper;

    public Store toStore(StoreEntity entity){
        return mapper.map(entity, Store.class);
    }

    public StoreEntity toEntity(Store store){
        return mapper.map(store, StoreEntity.class);
    }
}
