package com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity;


import com.martinachov.hexagonal.domain.model.Store;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StoreEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String location;

    // Constructor to initialize from a Store domain object
    public StoreEntity(Store store) {
        this.id = store.getId();
        this.name = store.getName();
        this.location = store.getLocation();
    }
}
