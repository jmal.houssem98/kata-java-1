package com.martinachov.hexagonal.infrastructure.adapters.output.persistence;

import com.martinachov.hexagonal.application.ports.output.OrderOutputPort;
import com.martinachov.hexagonal.domain.model.Order;
import com.martinachov.hexagonal.domain.service.MetricsService;
import com.martinachov.hexagonal.infrastructure.adapters.output.kafka.OrderKafkaProducer;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.OrderEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.mapper.OrderMapper;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class OrderPersistenceAdapter  implements OrderOutputPort {

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    @Autowired
    private OrderKafkaProducer orderKafkaProducer;

    @Autowired
    private MetricsService metricsService;


    @Override
    public Order saveOrder(Order order) {
        OrderEntity orderEntity = orderMapper.toEntity(order);
        orderRepository.save(orderEntity);
        // Increment the order counter
        metricsService.incrementOrderCounter();
        // Kafka Producer
        orderKafkaProducer.sendMessage(Long.toString(order.getProduct().getId()));
        return orderMapper.toOrder(orderEntity);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll().stream()
                .map(orderMapper::toOrder)
                .collect(Collectors.toList());
    }
}
