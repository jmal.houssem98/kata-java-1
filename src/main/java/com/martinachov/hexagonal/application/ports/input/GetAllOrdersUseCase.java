package com.martinachov.hexagonal.application.ports.input;

import com.martinachov.hexagonal.domain.model.Order;

import java.util.List;

public interface GetAllOrdersUseCase {

    List<Order> getAllOrders();
}
