package com.martinachov.hexagonal.application.ports.input;

import com.martinachov.hexagonal.domain.model.Store;

public interface CreateStoreUseCase {

    Store createStore(Store store);
}
