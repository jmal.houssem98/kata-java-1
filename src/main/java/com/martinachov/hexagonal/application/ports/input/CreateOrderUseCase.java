package com.martinachov.hexagonal.application.ports.input;

import com.martinachov.hexagonal.domain.model.Order;

public interface CreateOrderUseCase {

    Order createOrder(Order order);
}
