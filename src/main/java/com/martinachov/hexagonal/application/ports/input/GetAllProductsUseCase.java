package com.martinachov.hexagonal.application.ports.input;

import com.martinachov.hexagonal.domain.model.Product;

import java.util.List;

public interface GetAllProductsUseCase {

    List<Product> getAllProducts();
}
