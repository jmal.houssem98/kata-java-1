package com.martinachov.hexagonal.application.ports.output;

import com.martinachov.hexagonal.domain.model.Store;

import java.util.List;

public interface StoreOutputPort {

    List<Store> getAllStores();

    Store saveStore(Store store);
}
