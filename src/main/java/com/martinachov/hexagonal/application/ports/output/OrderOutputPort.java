package com.martinachov.hexagonal.application.ports.output;

import com.martinachov.hexagonal.domain.model.Order;

import java.util.List;

public interface OrderOutputPort {
    Order saveOrder(Order order);

    List<Order> getAllOrders();
}
