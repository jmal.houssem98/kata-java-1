package com.martinachov.hexagonal.application.ports.input;

import com.martinachov.hexagonal.domain.model.Store;

import java.util.List;

public interface GetAllStoresUseCase {

    List<Store> getAllStores();
}
