package com.martinachov.hexagonal.domain.service;

import com.martinachov.hexagonal.application.ports.input.CreateProductUseCase;
import com.martinachov.hexagonal.application.ports.input.GetAllProductsUseCase;
import com.martinachov.hexagonal.application.ports.input.GetProductUseCase;
import com.martinachov.hexagonal.application.ports.output.ProductOutputPort;
import com.martinachov.hexagonal.domain.exception.ProductNotFoundException;
import com.martinachov.hexagonal.domain.model.Product;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class ProductService implements CreateProductUseCase, GetProductUseCase, GetAllProductsUseCase {

    private final ProductOutputPort productOutputPort;

    @Override
    public List<Product> getAllProducts() {
        log.info("Return All Products");
        return productOutputPort.getAllProducts();
    }

    @Override
    public Product createProduct(Product product) {
        //In order to prevent updating old products
        Product product1=new Product();
        product1.setName(product.getName());
        product1.setDescription(product.getDescription());
        product1.setPrice(product.getPrice());
        product1.setStore(product.getStore());
        log.info("Created Product");
        return productOutputPort.saveProduct(product1);
    }

    @Override
    public Product getProductById(Long id) {
        log.info("Returning Product by ID");
        return productOutputPort.getProductById(id)
                .orElseThrow(() -> new ProductNotFoundException("No product found with this Id:" + id));
    }
}
