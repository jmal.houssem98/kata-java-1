package com.martinachov.hexagonal.domain.service;

import com.martinachov.hexagonal.application.ports.input.CreateOrderUseCase;
import com.martinachov.hexagonal.application.ports.input.GetAllOrdersUseCase;
import com.martinachov.hexagonal.application.ports.output.OrderOutputPort;
import com.martinachov.hexagonal.domain.model.Order;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class OrderService  implements CreateOrderUseCase, GetAllOrdersUseCase{

    private final OrderOutputPort orderOutputPort;

    @Override
    public List<Order> getAllOrders() {
        log.info("Return All Orders");
        return orderOutputPort.getAllOrders();
    }

    @Override
    public Order createOrder(Order order) {
        //In order to prevent updating old orders
        Order order1=new Order();
        order1.setQuantity(order.getQuantity());
        order1.setProduct(order.getProduct());
        log.info("Create Order");
        return orderOutputPort.saveOrder(order1);
    }
}
