package com.martinachov.hexagonal.domain.service;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

@Service
public class MetricsService {

    private final Counter orderCounter;

    public MetricsService(MeterRegistry meterRegistry) {
        this.orderCounter = Counter.builder("orders.created.total")
                .description("Total number of orders created")
                .register(meterRegistry);
    }

    public void incrementOrderCounter() {
        orderCounter.increment();
    }
}
