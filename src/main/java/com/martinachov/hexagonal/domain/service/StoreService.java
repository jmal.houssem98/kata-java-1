package com.martinachov.hexagonal.domain.service;

import com.martinachov.hexagonal.application.ports.input.CreateStoreUseCase;
import com.martinachov.hexagonal.application.ports.input.GetAllStoresUseCase;
import com.martinachov.hexagonal.application.ports.output.StoreOutputPort;
import com.martinachov.hexagonal.domain.model.Store;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class StoreService implements GetAllStoresUseCase, CreateStoreUseCase {

    private final StoreOutputPort storeOutputPort;

    @Override
    public Store createStore(Store store) {
        log.info("Create Store");
        return storeOutputPort.saveStore(store);
    }

    @Override
    public List<Store> getAllStores() {
        log.info("Return All Stores");
        return storeOutputPort.getAllStores();
    }
}
