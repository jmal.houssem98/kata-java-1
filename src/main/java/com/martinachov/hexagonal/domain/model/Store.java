package com.martinachov.hexagonal.domain.model;


import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Store {
    private Long id;
    private String name;
    private String location;
}
