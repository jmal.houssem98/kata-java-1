package com.martinachov.hexagonal.domain.service;

import com.martinachov.hexagonal.application.ports.output.OrderOutputPort;
import com.martinachov.hexagonal.domain.model.Order;
import com.martinachov.hexagonal.domain.model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class OrderServiceTest {

    private final OrderOutputPort orderOutputPort = Mockito.mock(OrderOutputPort.class);
    private final OrderService orderService = new OrderService(orderOutputPort);

    @BeforeEach
    public void setUp() {
        // Initialize mockito annotations
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateOrder() {
        // Create a sample product
        Product product = new Product();
        product.setId(1L);
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setPrice(100.0);

        // Create a sample order
        Order order = new Order();
        order.setId(1L);
        order.setProduct(product);
        order.setQuantity(2);

        // Use Mockito ArgumentMatchers to match any Order instance
        when(orderOutputPort.saveOrder(any(Order.class))).thenAnswer(invocation -> {
            Order savedOrder = invocation.getArgument(0);
            savedOrder.setId(1L); // Simulate saving and setting the ID
            return savedOrder;
        });

        // Call the method under test
        Order result = orderService.createOrder(order);

        // Assertions to validate the behavior
        assertNotNull(result, "Returned order should not be null");
        assertEquals(order.getProduct(), result.getProduct(), "Order product should match");
        assertEquals(order.getQuantity(), result.getQuantity(), "Order quantity should match");

        // Verify that saveOrder was called with any Order instance
        verify(orderOutputPort, times(1)).saveOrder(any(Order.class));
    }

    @Test
    public void testGetAllOrders() {
        // Create a sample product
        Product product = new Product();
        product.setId(1L);
        product.setName("Test Product");

        // Create a sample order
        Order order = new Order();
        order.setProduct(product);
        order.setQuantity(2);

        when(orderOutputPort.getAllOrders()).thenReturn(Collections.singletonList(order));

        List<Order> result = orderService.getAllOrders();
        assertEquals(1, result.size());
        assertEquals(order.getProduct(), result.get(0).getProduct());
        assertEquals(order.getQuantity(), result.get(0).getQuantity());

        verify(orderOutputPort, times(1)).getAllOrders();
    }
}
