package com.martinachov.hexagonal.domain.service;

import com.martinachov.hexagonal.application.ports.output.ProductOutputPort;
import com.martinachov.hexagonal.domain.model.Product;
import com.martinachov.hexagonal.domain.model.Store;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class ProductServiceTest {

    private final ProductOutputPort productOutputPort = Mockito.mock(ProductOutputPort.class);
    private final ProductService productService = new ProductService(productOutputPort);

    @Test
    public void testCreateProduct() {
        // Initialize mockito annotations
        MockitoAnnotations.openMocks(this);

        // Create a sample store
        Store store = new Store();
        store.setId(1L);
        store.setName("Test Store");
        store.setLocation("Test Location");

        // Create a sample product
        Product product = new Product();
        product.setId(1L);
        product.setName("product Name");
        product.setDescription("description");
        product.setPrice(10);
        product.setStore(store);

        // Use Mockito ArgumentMatchers to match any Product instance
        when(productOutputPort.saveProduct(any(Product.class))).thenAnswer(invocation -> {
            Product savedProduct = invocation.getArgument(0);
            savedProduct.setId(1L); // Simulate saving and setting the ID
            return savedProduct;
        });

        // Call the method under test
        Product result = productService.createProduct(product);

        // Assertions to validate the behavior
        assertNotNull(result, "Returned product should not be null");
        assertEquals(product.getName(), result.getName(), "Product name should match");
        assertEquals(product.getDescription(), result.getDescription(), "Product description should match");
        assertEquals(product.getPrice(), result.getPrice(), "Product price should match");
        assertEquals(product.getStore(), result.getStore(), "Product store should match");

        // Verify that saveProduct was called with any Product instance
        verify(productOutputPort, times(1)).saveProduct(any(Product.class));
    }


    @Test
    public void testGetAllProducts() {
        Product product = new Product();
        product.setName("Product 1");

        when(productOutputPort.getAllProducts()).thenReturn(Collections.singletonList(product));

        List<Product> result = productService.getAllProducts();
        assertEquals(1, result.size());
        assertEquals(product.getName(), result.get(0).getName());

        verify(productOutputPort, times(1)).getAllProducts();
    }
}
