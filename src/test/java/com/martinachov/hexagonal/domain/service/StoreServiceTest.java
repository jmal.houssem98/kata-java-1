package com.martinachov.hexagonal.domain.service;


import com.martinachov.hexagonal.application.ports.output.StoreOutputPort;
import com.martinachov.hexagonal.domain.model.Store;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class StoreServiceTest {

    private final StoreOutputPort storeOutputPort = Mockito.mock(StoreOutputPort.class);
    private final StoreService storeService = new StoreService(storeOutputPort);

    @Test
    public void testGetAllStores() {
        Store store = new Store();
        store.setName("store 1");

        when(storeOutputPort.getAllStores()).thenReturn(Collections.singletonList(store));

        List<Store> result = storeService.getAllStores();
        assertEquals(1, result.size());
        assertEquals(store.getName(), result.get(0).getName());

        verify(storeOutputPort, times(1)).getAllStores();
    }

    @Test
    public void testCreateStore() {
        Store store = new Store();
        store.setName("Product 1");
        store.setLocation("Location 1");

        when(storeOutputPort.saveStore(store)).thenReturn(store);

        Store result = storeService.createStore(store);
        assertEquals(store.getName(), result.getName());
        assertEquals(store.getLocation(), result.getLocation());

        verify(storeOutputPort, times(1)).saveStore(store);
    }
}
