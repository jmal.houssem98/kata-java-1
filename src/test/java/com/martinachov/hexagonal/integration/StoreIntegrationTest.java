package com.martinachov.hexagonal.integration;

import com.martinachov.hexagonal.domain.model.Store;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.StoreEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.StoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StoreIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private StoreRepository storeRepository;

    @BeforeEach
    void setUp() {
        storeRepository.deleteAll();

        Store store1 = new Store();
        store1.setId(1L);
        store1.setName("Test Store 1");
        store1.setLocation("Test Location 1");

        Store store2 = new Store();
        store2.setId(2L);
        store2.setName("Test Store 2");
        store2.setLocation("Test Location 2");

        storeRepository.save(new StoreEntity(store1));
        storeRepository.save(new StoreEntity(store2));
    }

    @Test
    void testCreateStore() {
        String baseUrl = "http://localhost:" + port + "/v1/stores";

        Store store = new Store();
        store.setId(3L);
        store.setName("New Store");
        store.setLocation("New Location");

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Store> request = new HttpEntity<>(store, headers);

        ResponseEntity<Store> response = restTemplate.postForEntity(baseUrl, request, Store.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getName()).isEqualTo("New Store");
        assertThat(response.getBody().getLocation()).isEqualTo("New Location");

        // Clean up
        storeRepository.deleteById(3L);
    }

    @Test
    void testGetAllStores() {
        String baseUrl = "http://localhost:" + port + "/v1/allstores";

        ResponseEntity<Store[]> response = restTemplate.getForEntity(baseUrl, Store[].class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().length).isEqualTo(2);
        assertThat(Arrays.stream(response.getBody()).anyMatch(store -> store.getName().equals("Test Store 1") && store.getLocation().equals("Test Location 1"))).isTrue();
        assertThat(Arrays.stream(response.getBody()).anyMatch(store -> store.getName().equals("Test Store 2") && store.getLocation().equals("Test Location 2"))).isTrue();
    }
}
