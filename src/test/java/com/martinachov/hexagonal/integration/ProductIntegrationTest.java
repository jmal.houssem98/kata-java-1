package com.martinachov.hexagonal.integration;

import com.martinachov.hexagonal.domain.model.Product;
import com.martinachov.hexagonal.domain.model.Store;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.ProductEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.entity.StoreEntity;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.ProductRepository;
import com.martinachov.hexagonal.infrastructure.adapters.output.persistence.repository.StoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;

    @BeforeEach
    void setUp() {
        productRepository.deleteAll();
        storeRepository.deleteAll();

        Store store = new Store();
        store.setId(1L);
        store.setName("Test Store");
        store.setLocation("Test Location");

        StoreEntity storeEntity = new StoreEntity(store);
        storeEntity = storeRepository.save(storeEntity);

        Product product1 = new Product();
        product1.setId(1L);
        product1.setName("Test Product 1");
        product1.setDescription("Description for product 1");
        product1.setPrice(100.0);
        product1.setStore(store);

        Product product2 = new Product();
        product2.setId(2L);
        product2.setName("Test Product 2");
        product2.setDescription("Description for product 2");
        product2.setPrice(200.0);
        product2.setStore(store);

        productRepository.save(new ProductEntity(product1));
        productRepository.save(new ProductEntity(product2));
    }

    @Test
    void testCreateProduct() {
        String baseUrl = "http://localhost:" + port + "/v1/products";

        // Create a Store object
        Store store = new Store();
        store.setId(1L);
        store.setName("Test Store");
        store.setLocation("Test Store");

        Product product = new Product();
        product.setId(1L);
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setPrice(100.0);
        product.setStore(store);

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Product> request = new HttpEntity<>(product, headers);

        ResponseEntity<Product> response = restTemplate.postForEntity(baseUrl, request, Product.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getName()).isEqualTo("Test Product");

        // Clean up
        productRepository.deleteAll();
    }

    @Test
    void testGetAllProducts() {
        String baseUrl = "http://localhost:" + port + "/v1/allproducts";

        ResponseEntity<Product[]> response = restTemplate.getForEntity(baseUrl, Product[].class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().length).isEqualTo(2);
        assertThat(Arrays.stream(response.getBody()).anyMatch(product -> "Test Product 1".equals(product.getName()))).isTrue();
        assertThat(Arrays.stream(response.getBody()).anyMatch(product -> "Test Product 2".equals(product.getName()))).isTrue();
    }
}
