package com.martinachov.hexagonal.performance;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
public class PerformanceTest {

    private static final String BASE_URL = "http://localhost:8080";
    private CloseableHttpClient httpClient;

    @Setup
    public void setup() {
        httpClient = HttpClients.createDefault();
    }

    @TearDown
    public void tearDown() throws IOException {
        httpClient.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testCreateProduct() throws IOException {
        HttpPost request = new HttpPost(BASE_URL + "/v1/products");
        String json = "{\"name\": \"Test Product\", \"description\": \"Test Description\", \"price\": 100.0, \"storeId\": 1}";
        request.setEntity(new StringEntity(json));
        request.setHeader("Content-type", "application/json");
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            response.getEntity().getContent().close();
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testGetProducts() throws IOException {
        HttpGet request = new HttpGet(BASE_URL + "/v1/products");
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            response.getEntity().getContent().close();
        }
    }

}
